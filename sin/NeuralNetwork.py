import numpy as np
from scipy.special import expit, softmax
from sklearn.metrics import mean_squared_error
from tensorflow.keras.losses import CategoricalCrossentropy
import random

class NeuralNetwork:
    def __init__(self, X, y, net_structure, loss_function, output_activation_function):
        self.X = X
        self.y = y
        self.net_structure = net_structure
        self.loss_function = loss_function
        self.output_activation_function = output_activation_function

    def relu(self, x):
        return np.maximum(0, x)

    def loss_and_activation_functions(self, mode, X, y=None):

        if mode == "Loss":   
            if self.loss_function == "mean_squared_error":
                return mean_squared_error(y,X)
            if self.loss_function == "categorical_crossentropy":
                return CategoricalCrossentropy()(y, X).numpy()

        elif mode == "Activation":
            if self.output_activation_function == "sigmoid":
                return expit(X)
            elif self.output_activation_function == "softmax":
                return softmax(X)
            elif self.output_activation_function == "tanh":
                return np.tanh(X)
            
        
    def forward(self, x):
        output = []
        for inputs in self.X:
            end = 0
            for j in range(len(self.net_structure) - 1):
                begin = end  
                end = begin + self.net_structure[j] * self.net_structure[j+1]
                    
                weights = np.reshape(x[begin:end], (self.net_structure[j], self.net_structure[j+1]))
                biases = x[end:end + self.net_structure[j+1]]
                inputs = np.dot(inputs, weights) + biases

                end = end + self.net_structure[j+1]
                if j != len(self.net_structure) - 2:
                    inputs = np.tanh(inputs)

            inputs = self.loss_and_activation_functions(mode = "Activation", X = inputs[0][0])
            output.append(inputs)

        loss =  self.loss_and_activation_functions(mode = "Loss", X = output, y = self.y)
        return loss, output