import numpy as np
from matplotlib import pyplot as plt
from EvolutionAlgorithms import EvolutionStrategies, Unit
from EvolutionAlgorithms import DifferentialEvolution
from NeuralNetwork import NeuralNetwork
import time
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
import os
import csv
from operator import attrgetter
from datetime import date
from Reg.src.rec import RegressionErrorCharacteristic

def get_dimension(net_structure):
    dim = 0
    for i in range(len(net_structure) - 1):
        dim = dim + (net_structure[i] * net_structure[i+1]) + net_structure[i+1]
    return dim

def mad(y_true, y_pred):
    n = len(y_pred)
    sum = 0
    for i in range(n):
        sum += np.abs(y_true[i]-y_pred[i])
    return sum/n

def plot_results(x, y, x_axis_title, y_axis_title, file_name, path, valid=False):
    if valid:
        plt.plot(x, y[0], x, y[1], x, y[2])
        plt.legend(["Valid", "DE", "ES"], fontsize=18)
    else:
        plt.plot(x, y[0], x, y[1])
        plt.legend(["DE", "ES"], fontsize=18)
    plt.xlabel(x_axis_title, fontsize=22)
    plt.ylabel(y_axis_title, fontsize=22)
    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    plt.tight_layout()

    if not os.path.exists(path):
        os.makedirs(path)

    plt.savefig(path+"\{}.png".format(file_name))
    #plt.show()
    plt.close()

def create_config_file(path, data):
    f = open(path+"\\config.txt", "w")
    for key, val in data.items():
        f.write("{}: {}\n".format(key, val))
    f.close()

def create_dump_file(path, data):
    row_list = [["Nr", "Units", "LossDE", "TimeDE", "R2DE", "MAD-DE", "GenDE", "LossES", "TimeES", "R2ES", "MAD-ES", "GenES"]]
    for i in range(len(data["Units"])):
        x = [i+1, data["Units"][i], data["Error"][0][i], data["Time"][0][i], data["R2"][0][i], data["MAD"][0][i], data["Generations"][0][i],
             data["Error"][1][i], data["Time"][1][i], data["R2"][1][i], data["MAD"][1][i], data["Generations"][1][i]]
 
        row_list.append(x)

    with open(path+"\\dump.csv", "w") as file:
        writer = csv.writer(file, delimiter=',', quoting = csv.QUOTE_MINIMAL)
        writer.writerows(row_list)

def e_strategies(units, train, test, u_multi, o_multi):
    values, iter_time, genes, r2, gen, mads = [], [], [], [], [], []
    for it, N in enumerate(units):
        r2_diff = []
        start = time.time()
        u = max(1, (int)(np.round(N * u_multi)))
        o = max(1, (int)(np.round(u * o_multi)))
        evo = EvolutionStrategies(dim = get_dimension(train.net_structure), u = u, o = o, lamb = N)
        population = evo.initialize_population(train)
        
        g = 0     
        while True:
            offspring = []
            for _ in range(N):
                parents = evo.marriage(population)
                s = evo.s_recombination(parents)
                x = evo.x_recombination(parents)
                s = evo.s_mutation(s)
                x = evo.x_mutation(x,s)
                f, _ = train.forward(x)
                offspring.append(Unit(x,f,s))
            population = evo.selection(offspring, population)

            _, output = test.forward(x = population[0].x)
            r2s = r2_score(test.y, output)
            mad_score = mad(test.y, output)

            if r2s >= 0.85 or population[0].f <= 0.05 or g == 300:
                break

            if g > 150:
                r2_diff.append(r2s)

            if len(r2_diff) == 50:
                m1 = np.mean(r2_diff[:25])
                m2 = np.mean(r2_diff[25:])
                if m1 == m2:
                    break
                r2_diff = r2_diff[25:]
            
            g += 1

        end = time.time()
        values.append(population[0].f)
        iter_time.append(end-start)
        gen.append(g)
        genes.append(population[0].x)
        r2.append(r2s)
        mads.append(mad_score)
        print("ES - I: {}, G: {}, V: {}, Time: {}, R2: {}, MAD: {}".format(it, g, population[0].f, end - start, r2s, mad_score))
    return values, iter_time, r2, gen, genes, mads

def diff_evolution(units, train, test, f, c):
    values, iter_time, genes, r2, gen, mads = [], [], [], [], [], []
    for it, N in enumerate(units):
        r2_diff = []
        start = time.time()
        evo = DifferentialEvolution(dim = get_dimension(train.net_structure), C = c, F = f, N = N)
        population = evo.initialize_population(train)
        
        g = 0    
        while True:
            for i in range(evo.N):
                x = population[i]
                u = evo.mutation(population, i, x)
                o = evo.recombination(x, u) 
                f, _ = train.forward(o)
                if f < x.f:
                    population[i].x = o
                    population[i].f = f

            best_unit = min(population, key=attrgetter('f'))
            _, output = test.forward(x = best_unit.x)
            r2s = r2_score(test.y, output)
            mad_score = mad(test.y, output)

            if r2s >= 0.85 or best_unit.f <= 0.05 or g == 300:
                break

            if g > 150:
                r2_diff.append(r2s)

            if len(r2_diff) == 50:
                m1 = np.mean(r2_diff[:25])
                m2 = np.mean(r2_diff[25:])
                if m1 == m2:
                    break
                r2_diff = r2_diff[25:]
            
            g += 1

        end = time.time()
        values.append(best_unit.f)
        iter_time.append(end-start)
        gen.append(g)
        genes.append(population[0].x)
        r2.append(r2s)
        mads.append(mad_score)
        print("DE - I: {}, G: {}, V: {}, Time: {}, R2: {}, MAD: {}".format(it, g, best_unit.f, end - start, r2s, mad_score))
    return values, iter_time, r2, gen, genes, mads

def main():
    X = np.linspace(-2*np.pi, 2*np.pi, 500)
    y = np.sin(X)
    data = {
        "net_structure": [1,16,8,1],
        "units" : [4, 8, 12, 15, 20, 25, 32, 50],
        "iterations" : 30,
        "F" : 1.0,
        "C" : 0.5,  
        "u" : 0.33,
        "o" : 0.05,
        "train_test_ratio": 0.7,
        "loss_function": "mean_squared_error",
        "activ_funtion": "tanh",
        "path": "Results\\{}".format("Units"),
        "date": date.today()
    }
    X_train, X_test, y_train, y_test = train_test_split(X,y, train_size=data["train_test_ratio"])
    train = NeuralNetwork(X_train, y_train, data["net_structure"], data["loss_function"], data["activ_funtion"])
    test = NeuralNetwork(X_test, y_test, data["net_structure"], data["loss_function"], data["activ_funtion"])
    valid = NeuralNetwork(X, y, data["net_structure"], data["loss_function"], data["activ_funtion"])

    methods = (("DE", diff_evolution, data["F"], data["C"]), ("ES", e_strategies, data["u"], data["o"]))
    y_val, y_time, y_r2, y_gen, rec, y_mad, valid_y = [], [], [], [], [], [], [y]
    for title, method, p1, p2 in methods:
        n = len(data["units"])
        mean_values = np.empty((data["iterations"], n))
        mean_times = np.empty((data["iterations"], n))
        mean_r2 = np.empty((data["iterations"], n))
        mean_gen = np.empty((data["iterations"], n))
        mean_mad = np.empty((data["iterations"], n))
        genes = []
        for i in range(data["iterations"]):
            s = time.time()
            v, t, r2, g, x, mads = method(data["units"], train, test, p1, p2)
            mean_values[i] = v
            mean_times[i] = t
            mean_r2[i] = r2
            mean_gen[i] = g
            mean_mad[i] = mads
            genes.append(x)
            e = time.time()
            print("I: {}, M: {}, T: {}".format(i, title, e-s))
        y_val.append(np.mean(mean_values, axis=0))
        y_time.append(np.mean(mean_times, axis=0))
        y_r2.append(np.mean(mean_r2, axis=0))
        y_gen.append(np.mean(mean_gen, axis=0))
        y_mad.append(np.mean(mean_mad, axis=0))

        genes = sum(genes,[])
        index = np.argmin(mean_values)
        _, output = valid.forward(genes[index])
        rec.append(RegressionErrorCharacteristic(y, output))
        valid_y.append(output)

    results = {
        "Units": data["units"],
        "Error": y_val,
        "Time": y_time,
        "R2": y_r2,
        "MAD": y_mad, 
        "Generations": y_gen
    }

    plot_results(data["units"], y_val, x_axis_title="Liczba potomków", y_axis_title="Średni błąd",
                 file_name="Loss", path=data["path"])

    plot_results(data["units"], y_time, x_axis_title="Liczba potomków", y_axis_title="Średni czas",
                 file_name="Time", path=data["path"])

    plot_results(data["units"], y_r2, x_axis_title="Liczba potomków", y_axis_title="Średnia wartość R2",
                 file_name="R2", path=data["path"])
    
    plot_results(data["units"], y_mad, x_axis_title="Liczba potomków", y_axis_title="Średnia wartość MAD",
                 file_name="MAD", path=data["path"])

    plot_results(X, valid_y, x_axis_title="X", y_axis_title="Y", file_name="sinus", path=data["path"], valid=True)
    
    rec[0].plot_rec(title="REC CURVE DE", path=data["path"])
    rec[1].plot_rec(title="REC CURVE ES", path=data["path"])
    
    create_config_file(path=data["path"], data=data)
    create_dump_file(path=data["path"], data=results)

if __name__ == "__main__":
    main()