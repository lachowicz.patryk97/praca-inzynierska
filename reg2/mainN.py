import numpy as np
from matplotlib import pyplot as plt
from EvolutionAlgorithms import EvolutionStrategies, Unit
from EvolutionAlgorithms import DifferentialEvolution
from NeuralNetwork import NeuralNetwork
import time
from sklearn.model_selection import train_test_split
import os
import csv
from operator import attrgetter
from sklearn.metrics import mean_squared_error
from scipy.stats import zscore

def load_data():
    '''
    Funkcja wczytująca dane z pliku csv wraz z preprocessingiem.
    Zamiana miesięcy i dni na format 1-of-C, konwersja stringów na float,
    zastosowanie normalizacji zscore danych wejściowych oraz normalizacji
    logarytmicznej danych wyjściowych.
    '''
    X = []
    y = []
    months = {
        'jan': [1,0,0,0,0,0,0,0,0,0,0,0],
        'feb': [0,1,0,0,0,0,0,0,0,0,0,0],
        'mar': [0,0,1,0,0,0,0,0,0,0,0,0],
        'apr': [0,0,0,1,0,0,0,0,0,0,0,0],
        'may': [0,0,0,0,1,0,0,0,0,0,0,0],
        'jun': [0,0,0,0,0,1,0,0,0,0,0,0],
        'jul': [0,0,0,0,0,0,1,0,0,0,0,0],
        'aug': [0,0,0,0,0,0,0,1,0,0,0,0],
        'sep': [0,0,0,0,0,0,0,0,1,0,0,0],
        'oct': [0,0,0,0,0,0,0,0,0,1,0,0],
        'nov': [0,0,0,0,0,0,0,0,0,0,1,0],
        'dec': [0,0,0,0,0,0,0,0,0,0,0,1]
    }
    
    days = {
        'mon': [1,0,0,0,0,0,0],
        'tue': [0,1,0,0,0,0,0],
        'wed': [0,0,1,0,0,0,0],
        'thu': [0,0,0,1,0,0,0],
        'fri': [0,0,0,0,1,0,0],
        'sat': [0,0,0,0,0,1,0],
        'sun': [0,0,0,0,0,0,1]
    }
    
    with open('forestfires.csv', newline='') as file:
        reader = csv.reader(file, delimiter=',')
        file.readline()
        for row in reader:
            row[2:3] = months[row[2]]
            row[14:15] = days[row[14]]
            row = list(map(float, row))
            row[:-1] = list(zscore(row[:-1]))
            
            X.append(row[25:-1]) # Wyciągnięcie 4 ostatnich argumentów wejściowych (dane pogodowe)
            y.append(row[-1])
            
    for i in range(len(y)):
        y[i] = np.log(y[i]+1)

    return X,y

def get_dimension(net_structure):
    '''
    Funkcja wyliczająca długości wektorów cech każdego osobnika. 
    Jako argument podana jest struktura sieci.
    '''
    
    dim = 0
    for i in range(len(net_structure) - 1):
        dim = dim + (net_structure[i] * net_structure[i+1]) + net_structure[i+1]
    return dim

def plot_results(x, y, x_axis_title, y_axis_title, file_name, path, valid=False):
    '''
    Funkcja rysująca wykresy. 
    Argumenty:
        x,y - dane do wyrysowania
        x_axis_title, y_axis_title - tytuły osi X i Y
        file_name - nazwa pliku z wykresem
        path - ścieżka zapisu pliku z wykresem 
    '''
    
    plt.plot(x, y[0], x, y[1])
    plt.legend(["DE", "ES"], fontsize=18)
        
    plt.xlabel(x_axis_title, fontsize=22)
    plt.ylabel(y_axis_title, fontsize=22)
    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    plt.tight_layout()

    if not os.path.exists(path):
        os.makedirs(path)

    plt.savefig(path+"\{}.png".format(file_name))
    #plt.show()
    plt.close()

def create_config_file(path, data):
    '''
    Funkcja tworząca plik txt z aktualną konfiguracją eksperymentu.
    Argumenty:
        path - Ścieżka do zapisu pliku txt
        data - słownik z danymi do zapisu w pliku  
    '''
    
    f = open(path+"\\config.txt", "w")
    for key, val in data.items():
        f.write("{}: {}\n".format(key, val))
    f.close()

def create_dump_file(path, data):
    '''
    Funkcja tworząca plik csv z wynikami eksperymentu.
    Argumenty:
        path - Ścieżka do zapisu pliku csv
        data - słownik z danymi do zapisu w pliku  
    '''
    
    row_list = [["Nr", "Network", "LossDE", "TimeDE", "MAD-DE", "GenDE", "LossES", "TimeES", "MAD-ES", "GenES"]]
    for i in range(len(data["Network"])):
        x = [i+1, data["Network"][i], data["Error"][0][i], data["Time"][0][i], data["MAD"][0][i], data["Generations"][0][i],
             data["Error"][1][i], data["Time"][1][i], data["MAD"][1][i], data["Generations"][1][i]]

        row_list.append(x)

    with open(path+"\\dump.csv", "w") as file:
        writer = csv.writer(file, delimiter=',', quoting = csv.QUOTE_MINIMAL)
        writer.writerows(row_list)

def mad(y_true, y_pred):
    n = len(y_pred)
    sum = 0
    for i in range(n):
        sum += np.abs(y_true[i]-y_pred[i])
    return sum/n

def e_strategies(units, train, test, u_multi, o_multi):
    '''
    Funkcja algorytmu strategii ewolucyjnych. Główna pętla iteruje po wartościach zmiennych train i test.
    
    Argumenty:
        units - liczba osobników rodzicielskich
        train - obiekt treningowej sieci neuronowej (lista)
        test - obiekt testowej sieci neuronowej (lista)
        u_multi - parametr sterujący wartością u algorytmu
        o_multi - parametr sterujący wartością o algorytmu
    
    Return:
        values - wartości straty (Loss) najlepszego osobnika każdej iteracji (lista)
        iter_time - łączny czas uczenia w każdej iteracji (lista)
        gen - liczba osiągniętych generacji w każdej iteracji (lista)
        mads - Mean Absolute Deviation (lista)
    '''
    
    values, iter_time, gen, mads = [], [], [], []
    for it, (train_net, test_net) in enumerate(zip(train,test)):
        ms_diff = []
        start = time.time()
        #Wyznaczenie wartości u oraz o, a następnie stworzenie instancji algorytmu i inicjalizacja populacji
        u = max(1, (int)(np.round(units * u_multi)))
        o = max(1, (int)(np.round(u * o_multi)))
        evo = EvolutionStrategies(dim = get_dimension(train_net.net_structure), u = u, o = o, lamb = units)
        population = evo.initialize_population(train_net)
        
        g = 0     
        while True:
            offspring = []
            for _ in range(units):
                parents = evo.marriage(population)
                s = evo.s_recombination(parents)
                x = evo.x_recombination(parents)
                s = evo.s_mutation(s)
                x = evo.x_mutation(x,s)
                f, _ = train_net.forward(x)
                offspring.append(Unit(x,f,s))
            population = evo.selection(offspring, population)

            _, output = test_net.forward(x = population[0].x)
            ms = mean_squared_error(test_net.y, output, squared=False)
            mad_score = mad(test_net.y, output)
            
            if ms < 1 or g == 400:
                break

            if g > 50:
                ms_diff.append(ms)
            
            # Sprawdzanie czy sieć nadal się uczy
            # Następuje w generacjach 100, 125, 150, 175 etc.
            if len(ms_diff) == 50:
                m1 = np.mean(ms_diff[:25])
                m2 = np.mean(ms_diff[25:])
                if np.abs(m1 - m2) <= 0.001:
                    break
                ms_diff = ms_diff[25:]
            
            g += 1 

        end = time.time()
        # Na wartości straty najlepszego osobnika wykonywana jest funkcja odwrócenia logarytmu
        val = np.exp(population[0].f) 
        values.append(val)
        iter_time.append(end-start)
        gen.append(g)
        mads.append(mad_score)
        print("ES - I: {}, G: {}, V: {}, Time: {}, MS: {}, MAD: {}".format(it, g, val, end - start, ms, mad_score))
    return values, iter_time, gen, mads

def diff_evolution(units, train, test, f, c):
    '''
    Funkcja algorytmu ewolucji różnicowej. Główna pętla iteruje po wartościach zmiennych train i test.
    
    Argumenty:
        units - liczba osobników rodzicielskich 
        train - obiekt treningowej sieci neuronowej (lista)
        test - obiekt testowej sieci neuronowej (lista)
        f - parametr F algorytmu
        c - parametr C algorytmu
    
    Return:
        values - wartości straty (Loss) najlepszego osobnika każdej iteracji (lista)
        iter_time - łączny czas uczenia w każdej iteracji (lista)
        gen - liczba osiągniętych generacji w każdej iteracji (lista)
        mad - Mean Absolute Deviation (lista)
    '''
    
    values, iter_time, gen, mads = [], [], [], []
    for it, (train_net, test_net) in enumerate(zip(train,test)):
        ms_diff = []
        start = time.time()
        # Stworzenie instancji algorytmu i inicjalizacja populacji 
        evo = DifferentialEvolution(dim = get_dimension(train_net.net_structure), C = c, F = f, N = units)
        population = evo.initialize_population(train_net)
        
        g = 0    
        while True:
            for i in range(evo.N):
                x = population[i]
                u = evo.mutation(population, i, x)
                o = evo.recombination(x, u) 
                f, output = train_net.forward(o)
                if f < x.f:
                    population[i].x = o
                    population[i].f = f

            best_unit = min(population, key=attrgetter('f'))
            _, output = test_net.forward(x = best_unit.x)
            ms = mean_squared_error(test_net.y, output, squared=False)
            mad_score = mad(test_net.y, output)

            if ms < 1 or g == 400:
                break

            if g > 50:
                ms_diff.append(ms)

            # Sprawdzanie czy sieć nadal się uczy
            # Następuje w generacjach 100, 125, 150, 175 etc.
            if len(ms_diff) == 50:
                m1 = np.mean(ms_diff[:25])
                m2 = np.mean(ms_diff[25:])
                if np.abs(m1 - m2) <= 0.001:
                    break
                ms_diff = ms_diff[25:]
            
            g += 1

        end = time.time()
        # Na wartości straty najlepszego osobnika wykonywana jest funkcja odwrócenia logarytmu
        val = np.exp(best_unit.f)
        values.append(val)
        iter_time.append(end-start)
        gen.append(g)
        mads.append(mad_score)
        print("DE - I: {}, G: {}, V: {}, Time: {}, MS: {}, MAD: {}".format(it, g, val, end - start, ms, mad_score))
    return values, iter_time, gen, mads

def main():
    X,y = load_data()
    # Słownik zawierający ustawienia eksperymentu
    data = {
        "net_structure": [[4,2,1], [4,4,1], [4,8,1], [4,16,1], [4,16,8,1], [4,32,8,1], [4,32,16,8,1], [4,32,32,16,1]],
        "units" : 15,
        "iterations" : 30,
        "F" : 1.0,
        "C" : 0.5,  
        "u" : 0.33,
        "o" : 0.05,
        "train_test_ratio": 0.7,
        "loss_function": "mean_squared_error",
        "activ_funtion": "sigmoid",
        "path": "Results\\{}".format("Network")
    }
    X_train, X_test, y_train, y_test = train_test_split(X,y, train_size=data["train_test_ratio"])

    train, test = [], []
    for i in range(len(data["net_structure"])):
        train.append(NeuralNetwork(X_train, y_train, data["net_structure"][i], data["loss_function"], data["activ_funtion"]))
        test.append(NeuralNetwork(X_test, y_test, data["net_structure"][i], data["loss_function"], data["activ_funtion"]))
    
    methods = (("DE", diff_evolution, data["F"], data["C"]), ("ES", e_strategies, data["u"], data["o"]))
    y_val, y_time, y_gen, y_mad = [], [], [], []
    
    # Pętla wywołująca drugą pętlę dla obu algorytmów
    for title, method, p1, p2 in methods:
        n = len(data["net_structure"])
        
        #Przygotowanie macierzy [ilośc iteracji x ilość parametrów (net_structure) ]
        mean_values = np.empty((data["iterations"], n))
        mean_times = np.empty((data["iterations"], n))
        mean_gen = np.empty((data["iterations"], n))
        mean_mad = np.empty((data["iterations"], n))
        
        '''
        Pętla iteracji, która wykonuje proces uczenia na wszystkich wartościach parametru (units),
        a następnie uśrednia wyniki ze wszystkich iteracji. Oba algorytmu zwracają listy, więc powyższe macierze uzupełniane są wierszami.
        Uśrednianie więc wykonywane jest na kolumnach, a następnie dołączane do listy. Listy te składają się z dwóch list uśrednionych,
        po jednej dla obu algorytmów.
        '''
        for i in range(data["iterations"]):
            s = time.time()
            v, t, g, mads = method(data["units"], train, test, p1, p2)
            mean_values[i] = v
            mean_times[i] = t
            mean_gen[i] = g
            mean_mad[i] = mads
            e = time.time()
            print("I: {}, M: {}, T: {}".format(i, title, e-s))
        y_val.append(np.mean(mean_values, axis=0))
        y_time.append(np.mean(mean_times, axis=0))
        y_gen.append(np.mean(mean_gen, axis=0))
        y_mad.append(np.mean(mean_mad, axis=0))

    # Słownik z wynikami eksperymentu
    results = {
        "Network": data["net_structure"],
        "Error": y_val,
        "Time": y_time,
        "MAD": y_mad,
        "Generations": y_gen
    }

    plot_x = [sum(net) - 5 for net in data["net_structure"]]
        
    plot_results(plot_x, y_val, x_axis_title="Liczba neuronów", y_axis_title="Średni błąd",
                 file_name="Loss", path=data["path"])

    plot_results(plot_x, y_time, x_axis_title="Liczba neuronów", y_axis_title="Średni czas",
                 file_name="Time", path=data["path"])

    plot_results(plot_x, y_mad, x_axis_title="Liczba potomków", y_axis_title="Średnia wartość MAD",
                 file_name="MAD", path=data["path"])

    create_config_file(path=data["path"], data=data)
    create_dump_file(path=data["path"], data=results)

if __name__ == "__main__":
    main()