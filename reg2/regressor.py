import csv
import numpy as np
from scipy.stats import zscore
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
import time
from matplotlib import pyplot as plt
import os
from sklearn.neural_network import MLPRegressor

def load_data():
    '''
    Funkcja wczytująca dane z pliku csv wraz z preprocessingiem.
    Zamiana miesięcy i dni na format 1-of-C, konwersja stringów na float,
    zastosowanie normalizacji zscore danych wejściowych oraz normalizacji
    logarytmicznej danych wyjściowych.
    '''
    X = []
    y = []
    months = {
        'jan': [1,0,0,0,0,0,0,0,0,0,0,0],
        'feb': [0,1,0,0,0,0,0,0,0,0,0,0],
        'mar': [0,0,1,0,0,0,0,0,0,0,0,0],
        'apr': [0,0,0,1,0,0,0,0,0,0,0,0],
        'may': [0,0,0,0,1,0,0,0,0,0,0,0],
        'jun': [0,0,0,0,0,1,0,0,0,0,0,0],
        'jul': [0,0,0,0,0,0,1,0,0,0,0,0],
        'aug': [0,0,0,0,0,0,0,1,0,0,0,0],
        'sep': [0,0,0,0,0,0,0,0,1,0,0,0],
        'oct': [0,0,0,0,0,0,0,0,0,1,0,0],
        'nov': [0,0,0,0,0,0,0,0,0,0,1,0],
        'dec': [0,0,0,0,0,0,0,0,0,0,0,1]
    }
    
    days = {
        'mon': [1,0,0,0,0,0,0],
        'tue': [0,1,0,0,0,0,0],
        'wed': [0,0,1,0,0,0,0],
        'thu': [0,0,0,1,0,0,0],
        'fri': [0,0,0,0,1,0,0],
        'sat': [0,0,0,0,0,1,0],
        'sun': [0,0,0,0,0,0,1]
    }
    
    with open('forestfires.csv', newline='') as file:
        reader = csv.reader(file, delimiter=',')
        file.readline()
        for row in reader:
            row[2:3] = months[row[2]]
            row[14:15] = days[row[14]]
            row = list(map(float, row))
            row[:-1] = list(zscore(row[:-1]))
            
            X.append(row[25:-1]) # Wyciągnięcie 4 ostatnich argumentów wejściowych (dane pogodowe)
            y.append(row[-1])
            
    for i in range(len(y)):
        y[i] = np.log(y[i]+1)

    return X,y

def plot_results(x, y, x_axis_title, y_axis_title, file_name, path):
    '''
    Funkcja rysująca wykresy. 
    Argumenty:
        x,y - dane do wyrysowania
        x_axis_title, y_axis_title - tytuły osi X i Y
        file_name - nazwa pliku z wykresem
        path - ścieżka zapisu pliku z wykresem 
    '''
    
    plt.plot(x, y[0], x, y[1])
    plt.legend(["DE", "ES"], fontsize=18)
        
    plt.xlabel(x_axis_title, fontsize=22)
    plt.ylabel(y_axis_title, fontsize=22)
    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    plt.tight_layout()

    if not os.path.exists(path):
        os.makedirs(path)

    plt.savefig(path+"\{}.png".format(file_name))
    #plt.show()
    plt.close()
    
def main():
    X,y = load_data()
    X_train, X_test, y_train, y_test = train_test_split(X,y, train_size=0.75)
    regr = MLPRegressor(hidden_layer_sizes=(8,1), activation="logistic", max_iter=300, learning_rate='adaptive', solver='lbfgs', random_state=100)
    regr.fit(X_train, y_train)
    print(regr.loss_)
    print(regr.n_iter_)
    pred = regr.predict(X_test)
    pred = np.round(pred, 2)
    y_test = np.round(y_test, 2)
    xd = list(zip(y_test,pred))
    print(xd)
    print(regr.score(X_test,y_test))
    

if __name__ == "__main__":
    main()