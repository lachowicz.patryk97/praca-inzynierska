import numpy as np
from scipy.special import expit, softmax
from sklearn.metrics import mean_squared_error
from tensorflow.keras.losses import CategoricalCrossentropy
import random

class NeuralNetwork:
    def __init__(self, X, y, net_structure, loss_function, output_activation_function):
        self.X = X
        self.y = y
        self.net_structure = net_structure
        self.loss_function = loss_function
        self.output_activation_function = output_activation_function
        
    def loss_functions(self, y_true, y_pred):
        if self.loss_function == "mean_squared_error":
            return mean_squared_error(y_true, y_pred, squared=False)
        elif self.loss_function == "categorical_crossentropy":
            return CategoricalCrossentropy()(y_true, y_pred).numpy()     
                 
    def activation_functions(self, X, func=None):
        if func == None:
            func = self.output_activation_function
            
        if func == "sigmoid":
            return expit(X)
        elif func == "softmax":
            return softmax(X)
        elif func == "tanh":
            return np.tanh(X)
        elif func == "relu":
            return np.maximum(0, X)
            
    def forward(self, x):
        output = []
        for inputs in self.X:
            end = 0
            for j in range(len(self.net_structure) - 1):
                begin = end  
                end = begin + self.net_structure[j] * self.net_structure[j+1]
                    
                weights = np.reshape(x[begin:end], (self.net_structure[j], self.net_structure[j+1]))
                biases = x[end:end + self.net_structure[j+1]]
                inputs = np.dot(inputs, weights) + biases

                end = end + self.net_structure[j+1]
                if j != len(self.net_structure) - 2:
                    inputs = self.activation_functions(X=inputs)

            output.append(inputs)

        if np.any(np.isnan(output)) or np.any(np.isinf(output)):
            print("Coś poszło nie tak")
            
        loss =  self.loss_functions(y_true=self.y, y_pred=output)
        return loss, output