net_structure: [[4, 2, 3], [4, 4, 3], [4, 8, 3], [4, 8, 8, 3], [4, 16, 8, 3], [4, 16, 16, 3], [4, 32, 16, 8, 3], [4, 32, 32, 16, 3]]
units: 15
iterations: 30
F: 1.0
C: 0.5
u: 0.33
o: 0.05
train_test_ratio: 0.8
loss_function: categorical_crossentropy
activ_funtion: softmax
path: Results\Network
date: 2021-03-03
