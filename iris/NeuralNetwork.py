import numpy as np
from scipy.special import expit, softmax
from sklearn.metrics import mean_squared_error
from tensorflow.keras.losses import CategoricalCrossentropy, Reduction
import random

class NeuralNetwork:
    def __init__(self, X, y, net_structure, loss_function, output_activation_function):
        self.X = X
        self.y = y
        self.net_structure = net_structure
        self.loss_function = loss_function
        self.output_activation_function = output_activation_function

        self.create_labels()

    def create_labels(self):
        self.labels = []
        for i in range(len(self.X)):
            temp = []
            for j in range(self.net_structure[-1]):
                if j == self.y[i]:
                    temp.append(1)
                else:
                    temp.append(0)
            self.labels.append(temp)

    def relu(self, x):
        return np.maximum(0, x)

    def loss_and_activation_functions(self, mode, X, y=None):
        if mode == "Loss":   
            if self.loss_function == "mean_squared_error":
                return mean_squared_error(y,X)
            elif self.loss_function == "categorical_crossentropy":
                return CategoricalCrossentropy()(y, X).numpy()
            elif self.loss_function == None:
                return X

        elif mode == "Activation":
            if self.output_activation_function == "sigmoid":
                return expit(X)
            elif self.output_activation_function == "softmax":
                return softmax(X)
            elif self.output_activation_function == None:
                return X
        
    def forward(self, x):
        output = []
        predictions = []
        for inputs in self.X:
            end = 0
            for j in range(len(self.net_structure) - 1):
                begin = end  
                end = begin + self.net_structure[j] * self.net_structure[j+1]
                    
                weights = np.reshape(x[begin:end], (self.net_structure[j], self.net_structure[j+1]))
                biases = x[end:end + self.net_structure[j+1]]
                inputs = np.dot(inputs, weights) + biases

                end = end + self.net_structure[j+1]
                if j != len(self.net_structure) - 2:
                    inputs = self.relu(inputs)

            inputs = self.loss_and_activation_functions(mode = "Activation", X = inputs)
            output.append(inputs.tolist())
            predictions.append(np.argmax(inputs))

        loss = self.loss_and_activation_functions(mode = "Loss", X = output, y = self.labels)
        return loss, output, predictions