import numpy as np
import random
import math
from sklearn.metrics import accuracy_score

class Unit:
    def __init__(self,x,f,acc,s=None):
        self.x = x
        self.f = f
        self.s = s
        self.acc = acc


class EvolutionStrategies:
    def __init__(self, dim, u, o, lamb):
        self.d = dim
        self.u = u
        self.o = o
        self.lamb = lamb
    
    def initialize_population(self, neural_network):
        random.seed()
        population = []
        
        for _ in range(self.u):
            x = 0.3 * np.random.randn(self.d)
            s = 0.83 * np.random.randn(self.d) + 2.6
            f, output, predictions = neural_network.forward(x)
            acc = accuracy_score(neural_network.y, predictions)
            population.append(Unit(x.copy(),f,acc,s.copy()))
        return population

    def marriage(self, population):
        parents=np.zeros(self.o).tolist()
        selected_individuals=np.random.permutation(len(population))
        for i in range(self.o):
            parents[i]=population[selected_individuals[i]]
        return parents
    
    def s_recombination(self, parents):
        n = len(parents[0].s)
        s = np.zeros(n)
        for i in range(n):
            feature_list = []
            for j in range(len(parents)):
                feature_list.append(parents[j].s[i])
            s[i] = np.mean(feature_list)
        return s

    def x_recombination(self, parents):
        n = len(parents[0].x)
        x = np.zeros(n)
        for i in range(n):
            feature_list = []
            for j in range(len(parents)):
                feature_list.append(parents[j].x[i])
            x[i] = np.mean(feature_list)
        return x

    def s_mutation(self, s):
        n = len(s)
        out_s = np.zeros(n)
        c = 1
        tau_z = math.exp((c/math.sqrt(2*n))*np.random.normal(loc=0.0,scale=1.0))
        tau = c/(math.sqrt(2 * math.sqrt(n)))
        for i in range(n):
            out_s[i] = tau_z * s[i] * math.exp(tau * np.random.normal(loc=0.0,scale=1.0))
        return out_s

    def x_mutation(self, x, s):
        n = len(x)
        out_x = np.zeros(n)
        for i in range(n):
            out_x[i] = x[i] + s[i] * np.random.normal(loc=0.0,scale=1.0)
        return out_x

    def selection(self, offspring, population):
        new_population=offspring
        new_population.extend(population)
        new_population.sort(key=lambda new_population: new_population.acc, reverse=True)
        new_population=new_population[:self.u]
        return new_population


class DifferentialEvolution:
    def __init__(self, dim, C, F, N):
        self.d = dim
        self.C = C
        self.F = F
        self.N = N
        
    def initialize_population(self, neural_network):
        random.seed()
        population = []
        
        for _ in range(self.N):
            x = 0.3 * np.random.randn(self.d)
            f, output, predictions = neural_network.forward(x)
            acc = accuracy_score(neural_network.y, predictions)
            population.append(Unit(x.copy(),f,acc))
        return population  

    def mutation(self, population, index, x1):
        ''' 
        Metoda mutacji różnicowej

        Losuje 2 osobników z populacji z pominięciem osobnika bazowego, którego indeks zawiera zmienna index. 

        '''
        n = len(population)
        indexes = list(range(0,n))
        indexes.remove(index)
        random.shuffle(indexes)
        u = np.zeros(self.d)

        x2 = population[indexes.pop(0)]
        x3 = population[indexes.pop(0)]

        for i in range(self.d):
            u[i] = x1.x[i] + self.F * (x2.x[i] - x3.x[i])
        return u

    def recombination(self, x, u):
        o = np.zeros(self.d)
        for i in range(self.d):
            val = random.random()
            if val <= self.C:
                o[i] = u[i]
            else:
                o[i] = x.x[i]
        return o