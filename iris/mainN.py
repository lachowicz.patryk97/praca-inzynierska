import numpy as np
from matplotlib import pyplot as plt
from EvolutionAlgorithms import EvolutionStrategies, Unit, DifferentialEvolution
from NeuralNetwork import NeuralNetwork
import time
from sklearn import datasets
from sklearn.metrics import accuracy_score, precision_score, roc_auc_score
from sklearn.model_selection import train_test_split
import os
import csv
from operator import attrgetter
from datetime import date

def get_dimension(net_structure):
    dim = 0
    for i in range(len(net_structure) - 1):
        dim = dim + (net_structure[i] * net_structure[i+1]) + net_structure[i+1]
    return dim

def plot_results(x, y, x_axis_title, y_axis_title, file_name, path):
    plt.plot(x, y[0], x, y[1])
    plt.legend(["DE", "ES"], fontsize=18)
    plt.xlabel(x_axis_title, fontsize=22)
    plt.ylabel(y_axis_title, fontsize=22)
    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    plt.tight_layout()

    if not os.path.exists(path):
        os.makedirs(path)

    plt.savefig(path+"\{}.png".format(file_name))
    #plt.show()
    plt.close()

def create_config_file(path, data):
    f = open(path+"\\config.txt", "w")
    for key, val in data.items():
        f.write("{}: {}\n".format(key, val))
    f.close()

def create_dump_file(path, data):
    row_list = [["Nr", "Network", "Loss", "Time", "Gen", "Acc", "Prec", "AUC"]]
    for i in range(len(data["Network"])):
        x = [i+1, data["Network"][i], data["Error"][0][i], data["Time"][0][i], data["Generations"][0][i], data["Accuracy"][0][i], data["Precision"][0][i], data["Auc"][0][i]]
        y = ["", data["Network"][i], data["Error"][1][i], data["Time"][1][i], data["Generations"][1][i], data["Accuracy"][1][i], data["Precision"][1][i], data["Auc"][1][i]]
        row_list.append(x)
        row_list.append(y)

    with open(path+"\\dump.csv", "w") as file:
        writer = csv.writer(file, delimiter=',', quoting = csv.QUOTE_MINIMAL)
        writer.writerows(row_list)
    

def e_strategies(units, train, test, u_multi, o_multi):
    values, iter_time, gen = [], [], []
    accuracy, precision, auc = [],[],[]
    for it, (train_net, test_net) in enumerate(zip(train,test)):
        start = time.time()
        u = max(1, (int)(np.round(units * u_multi)))
        o = max(1, (int)(np.round(u * o_multi)))
        evo = EvolutionStrategies(dim = get_dimension(train_net.net_structure), u = u, o = o, lamb = units)
        population = evo.initialize_population(train_net)
        
        acc_diff = []
        g = 0
        while True:
            offspring = []
            for _ in range(evo.lamb):
                parents = evo.marriage(population)
                s = evo.s_recombination(parents)
                x = evo.x_recombination(parents)
                s = evo.s_mutation(s)
                x = evo.x_mutation(x,s)
                f, _, predictions = train_net.forward(x)
                acc = accuracy_score(train_net.y, predictions)
                offspring.append(Unit(x,f,acc,s))
            population = evo.selection(offspring, population)  

            _, output, predictions = test_net.forward(x = population[0].x)
            acc = accuracy_score(test_net.y, predictions)
            if acc >= 0.95:
                break

            if g > 150:
                acc_diff.append(acc)

            if len(acc_diff) == 50:
                m1 = np.mean(acc_diff[:25])
                m2 = np.mean(acc_diff[25:])
                if m1 == m2:
                    break
                acc_diff = acc_diff[25:]

            g += 1

        end = time.time()
        values.append(population[0].f)
        iter_time.append(end-start)
        gen.append(g)
        accuracy.append(acc)
        precision.append(precision_score(test_net.y, predictions, average="macro", zero_division=0))  
        auc.append(roc_auc_score(test_net.y, output, multi_class="ovr"))
        print("ES - I: {}, G: {}, V: {}, Time: {}, Acc: {}".format(it, g, population[0].f, end - start, acc))

    return values, iter_time, accuracy, precision, auc, gen

def diff_evolution(units, train, test, f, c):
    values, iter_time, gen = [], [], []
    accuracy, precision, auc = [],[],[]
    for it, (train_net, test_net) in enumerate(zip(train,test)):
        acc_diff = []
        start = time.time()
        evo = DifferentialEvolution(dim = get_dimension(train_net.net_structure), C = c, F = f, N = units)
        population = evo.initialize_population(train_net)
        
        g = 0     
        while True:
            for i in range(evo.N):
                x = population[i]
                u = evo.mutation(population, i, x)
                o = evo.recombination(x, u) 
                f, _, predictions  = train_net.forward(o)
                acc = accuracy_score(train_net.y, predictions)
                if acc > x.acc:
                    population[i].x = o
                    population[i].f = f
                    population[i].acc = acc

            best_unit = max(population, key=attrgetter('acc'))
            _, output, predictions = test_net.forward(x = best_unit.x)
            acc = accuracy_score(test_net.y, predictions)
            if acc >= 0.95:
                break

            if g > 150:
                acc_diff.append(acc)

            if len(acc_diff) == 50:
                m1 = np.mean(acc_diff[:25])
                m2 = np.mean(acc_diff[25:])
                if m1 == m2:
                    break
                acc_diff = acc_diff[25:]

            g += 1

        end = time.time()
        values.append(best_unit.f)
        iter_time.append(end-start)
        gen.append(g)
        accuracy.append(acc)
        precision.append(precision_score(test_net.y, predictions, average="macro", zero_division=0))
        auc.append(roc_auc_score(test_net.y, output, multi_class="ovr"))
        print("DE - I: {}, G: {}, V: {}, Time: {}, Acc: {}".format(it, g, best_unit.f, end - start, acc))

    return values, iter_time, accuracy, precision, auc, gen

def main():
    iris = datasets.load_iris()
    X = iris.data
    y = iris.target

    data = {
        "net_structure": [[4,2,3], [4,4,3], [4,8,3], [4,8,8,3], [4,16,8,3], [4,16,16,3], [4,32,16,8,3], [4,32,32,16,3]],
        "units" : 15,
        "iterations" : 30,
        "F" : 1.0,
        "C" : 0.5,  
        "u" : 0.33,
        "o" : 0.05,
        "train_test_ratio": 0.8,
        "loss_function": "categorical_crossentropy",
        "activ_funtion": "softmax",
        "path": "Results\\{}".format("Network"),
        "date": date.today()
    }

    X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=data["train_test_ratio"])

    train = []
    test = []
    for i in range(len(data["net_structure"])):
        train.append(NeuralNetwork(X_train, y_train, data["net_structure"][i], data["loss_function"], data["activ_funtion"]))
        test.append(NeuralNetwork(X_test, y_test, data["net_structure"][i], data["loss_function"], data["activ_funtion"]))

    methods = (("DE", diff_evolution, data["F"], data["C"]), ("ES", e_strategies, data["u"], data["o"]))

    y_val, y_time, y_acc, y_prec, y_auc, y_gen = [], [], [], [], [], []
    for title, method, p1, p2 in methods:
        n = len(data["net_structure"])
        mean_values = np.empty((data["iterations"], n))
        mean_times = np.empty((data["iterations"], n))
        mean_acc = np.empty((data["iterations"], n))
        mean_prec = np.empty((data["iterations"], n))
        mean_auc = np.empty((data["iterations"], n))
        mean_gen = np.empty((data["iterations"], n))
        for i in range(data["iterations"]):
            s = time.time()
            v, t, acc, prec, auc, g = method(data["units"], train, test, p1, p2)
            mean_values[i] = v
            mean_times[i] = t
            mean_acc[i] = acc
            mean_prec[i] = prec
            mean_auc[i] = auc
            mean_gen[i] = g
            e = time.time()
            print("I: {}, M: {}, T: {}".format(i, title, e-s))

        y_val.append(np.mean(mean_values, axis=0))
        y_time.append(np.mean(mean_times, axis=0))
        y_acc.append(np.mean(mean_acc, axis=0))
        y_prec.append(np.mean(mean_prec, axis=0))
        y_auc.append(np.mean(mean_auc, axis=0))
        y_gen.append(np.mean(mean_gen, axis=0))


    results = {
        "Network": data["net_structure"],
        "Error": y_val,
        "Time": y_time,
        "Accuracy": y_acc,
        "Precision": y_prec,
        "Auc": y_auc,
        "Generations": y_gen
    }

    plot_x = [sum(net) - 7 for net in data["net_structure"]]
    plot_results(plot_x, y_val, x_axis_title="Liczba neuronów", y_axis_title="Średni błąd",
                 file_name="Loss", path=data["path"])

    plot_results(plot_x, y_time, x_axis_title="Liczba neuronów", y_axis_title="Średni czas",
                 file_name="Time", path=data["path"])

    plot_results(plot_x, y_acc, x_axis_title = "Liczba neuronów",y_axis_title="Accuracy",
                 file_name="Accuracy", path=data["path"])

    plot_results(plot_x, y_prec, x_axis_title="Liczba neuronów", y_axis_title="Precision",
                 file_name="Precision", path=data["path"])

    plot_results(plot_x, y_auc, x_axis_title="Liczba neuronów", y_axis_title="AUC-ROC",
                 file_name="Auc", path=data["path"])

    create_config_file(path=data["path"], data=data)
    create_dump_file(path=data["path"], data=results)

if __name__ == "__main__":
    main()