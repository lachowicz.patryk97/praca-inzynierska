import numpy as np
from scipy.special import expit, softmax
from sklearn.metrics import mean_squared_error
from tensorflow.keras.losses import CategoricalCrossentropy
import random

class NeuralNetwork:
    def __init__(self, X, y, net_structure, loss_function, output_activation_function):
        self.X = X
        self.y = y
        self.net_structure = net_structure
        self.loss_function = loss_function
        self.output_activation_function = output_activation_function

    def relu(self, x):
        return np.maximum(0, x)

    def activation_functions(self, X):
        if self.output_activation_function == "sigmoid":
            return expit(X)
        elif self.output_activation_function == "softmax":
            return softmax(X)

    def loss_functions(self, y_true, y_pred):
        if self.loss_function == "mean_squared_error":
            return mean_squared_error(y_true,y_pred)
        if self.loss_function == "categorical_crossentropy":
            return CategoricalCrossentropy()(y_true, y_pred).numpy()

    def forward(self, x):
        outputs = []
        predictions = []
        for inputs in self.X:
            end = 0
            for j in range(len(self.net_structure) - 1):
                begin = end  
                end = begin + self.net_structure[j] * self.net_structure[j+1]
                    
                weights = np.reshape(x[begin:end], (self.net_structure[j], self.net_structure[j+1]))
                biases = x[end:end + self.net_structure[j+1]]
                inputs = np.dot(inputs, weights) + biases

                end = end + self.net_structure[j+1]

                inputs = self.relu(inputs) * (j != len(self.net_structure) - 2) + inputs * (j == len(self.net_structure) - 2)

                if j != len(self.net_structure) - 2:
                    inputs = self.relu(inputs)

            inputs = self.activation_functions(X = inputs)
            outputs.append(inputs)
            predictions.append(np.around(inputs[0]))
            
        loss = self.loss_functions(y_true=self.y, y_pred=outputs)
        return loss, predictions, outputs