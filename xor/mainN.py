import numpy as np
from matplotlib import pyplot as plt
from EvolutionAlgorithms import EvolutionStrategies, Unit, DifferentialEvolution
from NeuralNetwork import NeuralNetwork
import time
from sklearn.metrics import accuracy_score
import plotly.express as px
import os
import csv
import plotly.graph_objects as go
from operator import attrgetter
from datetime import date

def get_dimension(net_structure):
    dim = 0
    for i in range(len(net_structure) - 1):
        dim = dim + (net_structure[i] * net_structure[i+1]) + net_structure[i+1]
    return dim

def plot_results(x, y, x_axis_title, y_axis_title, file_name, path):
    plt.plot(x, y[0], x, y[1])
    plt.legend(["DE", "ES"], fontsize=16)
    plt.xlabel(x_axis_title, fontsize=22)
    plt.ylabel(y_axis_title, fontsize=22)
    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    plt.tight_layout()

    if not os.path.exists(path):
        os.makedirs(path)

    plt.savefig(path+"\{}.png".format(file_name))
    plt.show()
    plt.close()
    
def create_config_file(path, data):
    f = open(path+"\\config.txt", "w")
    for key, val in data.items():
        f.write("{}: {}\n".format(key, val))
    f.close()

def create_dump_file(path, data):
    row_list = [["Nr", "Network", "Loss", "Time", "Gen", "Acc"]]
    for i in range(len(data["Network"])):
        x = [i+1, data["Network"][i], data["Error"][0][i], data["Time"][0][i], data["Generations"][0][i], data["Acc"][0][i]]
        y = ["", data["Network"][i], data["Error"][1][i], data["Time"][1][i], data["Generations"][1][i], data["Acc"][1][i]]
        row_list.append(x)
        row_list.append(y)

    with open(path+"\\dump.csv", "w") as file:
        writer = csv.writer(file, delimiter=',', quoting = csv.QUOTE_MINIMAL)
        writer.writerows(row_list)

def e_strategies(units, network, u_multi, o_multi):
    values, iter_time, gen, accuracy = [], [], [], []
    
    for it, net in enumerate(network):    
        start = time.time()
        u = max(1, (int)(np.round(units * u_multi)))
        o = max(1, (int)(np.round(u * o_multi)))
        evo = EvolutionStrategies(dim = get_dimension(net.net_structure), u = u, o = o, lamb = units)
        population = evo.initialize_population(net)
        
        g = 0
        while True:
            offspring = []
            for i in range(evo.lamb):
                parents = evo.marriage(population)
                s = evo.s_recombination(parents)
                x = evo.x_recombination(parents)
                s = evo.s_mutation(s)
                x = evo.x_mutation(x,s)
                f, predictions, _ = net.forward(x)
                acc = accuracy_score(net.y, predictions)
                offspring.append(Unit(x,f,acc,s))
            population = evo.selection(offspring, population)  

            g += 1
            _, predictions, _ = net.forward(x = population[0].x)
            acc = accuracy_score(net.y, predictions)
            if (acc == 1 and population[0].f <= 0.1) or g == 300:
                break

        end = time.time()
        values.append(population[0].f)
        iter_time.append(end-start)
        gen.append(g)
        accuracy.append(acc)
        print("I: {}, G: {}, V: {}, Time: {}, Acc: {}".format(it+1, g, population[0].f, end - start, acc))
    return values, iter_time, gen, accuracy

def diff_evolution(units, network, f, c):
    values, iter_time, gen, accuracy = [], [], [], []
    
    for it, net in enumerate(network):  
        start = time.time()
        evo = DifferentialEvolution(dim = get_dimension(net.net_structure), F = f, C = c, N = units)
        population = evo.initialize_population(net)
        
        g = 0
        while True:
            for i in range(evo.N):
                x = population[i]
                u = evo.mutation(population, i, x)
                o = evo.recombination(x, u) 
                f, predictions, _ = net.forward(o)
                acc = accuracy_score(net.y, predictions)
                if acc < x.acc:
                    continue
                
                if f <= x.f:
                    population[i].x = o
                    population[i].f = f
                    population[i].acc = acc

            g += 1
            best_unit = min(population, key=attrgetter('f'))
            _, predictions, _ = net.forward(best_unit.x)
            acc = accuracy_score(net.y, predictions)
            if (acc == 1 and best_unit.f <= 0.15) or g == 300:
                break

        end = time.time()
        values.append(best_unit.f)
        iter_time.append(end-start)
        gen.append(g)
        accuracy.append(acc)
        print("I: {}, G: {}, V: {}, Time: {}, Acc: {}".format(it+1, g, best_unit.f, end - start, acc))
    return values, iter_time, gen, accuracy

def experiment(units, network):
    iterations = 30
    F = [0.05, 0.1, 0.15, 0.2, 0.25, 0.4, 0.5, 0.7, 0.9, 1]
    C = [0.05, 0.1, 0.15, 0.2, 0.25, 0.4, 0.5, 0.7, 0.9, 1]
    U = [0.05, 0.1, 0.2, 0.33, 0.5, 0.7, 0.8, 1, 1.25]
    O = [0.05, 0.1, 0.2, 0.25, 0.33, 0.5, 0.7, 0.8, 1]
    df = { "x":[],
            "y":[],
            "f": [],
            "c": []
        }
    for f in F:
        for c in C:
            mean_values = np.empty((iterations, len(units)))
            mean_times = np.empty((iterations, len(units)))
            for i in range(iterations):
                v, t, _ = diff_evolution(units, network, f, c)
                #v, t, _ = e_strategies(units, network, u, o)
                mean_values[i] = v
                mean_times[i] = t
                print("I:{}, f:{}, c:{}".format(i, f, c))
            df["x"].append(np.mean(mean_values))
            df["y"].append(np.mean(mean_times))
            df["f"].append(f)
            df["c"].append(c)

    row_list = [['f', 'c', 'Loss', 'Time']]
    for i in range(len(F) * len(C)):
        x = [df["f"][i], df["c"][i], df["x"][i], df["y"][i]]
        row_list.append(x)

    with open("Results\\DE_optimal_results.csv", "w") as file:
        writer = csv.writer(file, delimiter=',', quoting = csv.QUOTE_MINIMAL)
        writer.writerows(row_list)

    fig = px.scatter(df, x="x",y="y", labels=dict(x="Średni błąd", y="Średni czas"), hover_data=["f", "c"],
                     color_discrete_sequence=px.colors.qualitative.Set1)
    fig.update_traces(marker=dict(size=16, line=dict(width=2, color='DarkSlateGrey')),selector=dict(mode='markers'))

    fig.update_layout(font_size = 32)
    fig.show()

def main():
    X = [[0, 0],
         [0, 1],
         [1, 0],
         [1, 1]]
    y = [0, 1, 1, 0]
    data = {
        "net_structure": [[2,1,1], [2,2,1], [2,3,1], [2,4,1], [2,8,1], [2,8,4,1], [2,16,8,1], [2,32,16,1]],
        "units" : 15,
        "iterations" : 30,
        "F" : 1.0,
        "C" : 0.5,
        "u" : 0.33,
        "o" : 0.05,
        "loss_function": "mean_squared_error",
        "activ_funtion": "sigmoid",
        "path": "Results\\{}".format("Network"),
        "date": date.today()
    }

    network = []
    n = len(data["net_structure"])
    for i in range(n):
        network.append(NeuralNetwork(X, y, data["net_structure"][i], data["loss_function"], data["activ_funtion"]))

    methods = (("DE", diff_evolution, data["F"], data["C"]), ("ES", e_strategies, data["u"], data["o"]))

    y_val, y_time, y_gen, y_acc = [], [], [], []
    for title, method, p1, p2 in methods:
        
        mean_values = np.empty((data["iterations"], n))
        mean_times = np.empty((data["iterations"], n))
        mean_gens = np.empty((data["iterations"], n))
        mean_acc = np.zeros((data["iterations"], n))
        for i in range(data["iterations"]):
            v, t, g, acc = method(data["units"], network, p1, p2)
            mean_values[i] = v
            mean_times[i] = t
            mean_gens[i] = g
            mean_acc[i] = acc
            print("I: {}, M: {}".format(i, title))
        y_val.append(np.mean(mean_values, axis=0))
        y_time.append(np.mean(mean_times, axis=0))
        y_gen.append(np.mean(mean_gens, axis=0))
        y_acc.append(np.mean(mean_acc, axis=0))
        

    results = {
        "Network": data["net_structure"], 
        "Error": y_val,
        "Time": y_time,
        "Generations": y_gen,
        "Acc": y_acc
    }

    plot_x = [sum(net) - 3 for net in data["net_structure"]]
    plot_results(plot_x, y_val, x_axis_title="Liczba neuronów", y_axis_title="Średni błąd", file_name="Loss", path=data["path"])
    plot_results(plot_x, y_time, x_axis_title="Liczba neuronów", y_axis_title="Średni czas", file_name="Time", path=data["path"])
    plot_results(plot_x, y_acc, x_axis_title="Liczba neuronów", y_axis_title="Średnia dokładność", file_name="Acc", path=data["path"])

    create_config_file(path=data["path"], data=data)
    create_dump_file(path=data["path"], data=results)
if __name__ == "__main__":
    main()